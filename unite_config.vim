
NeoBundle "Shougo/unite.vim"
" nnoremap <C-p> :Unite -start-insert file_rec/async<cr>
" nnoremap <F4> :Unite -start-insert buffer<CR>
" call unite#filters#matcher_default#use(['matcher_fuzzy'])
" call unite#custom#source('file_rec,file_rec/async', 'ignore_pattern', '\(node_modules\|bower_components\|\.git\|\.hg\|\.svn\|\.env\|\.sass-cache\|\.ropeproject\|node_modules\|ai-cache\|virtualenv\|target\|build\|\.tmp\|gendoc\|tmp\)')




NeoBundle 'Shougo/vimfiler'
NeoBundle 'kien/ctrlp.vim'

let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.git|\.hg|\.svn|\.env|dist|\.sass-cache|\.ropeproject|node_modules|ai-cache|virtualenv|target|build|gendoc|out)$',
  \ 'file': '\v\.(exe|so|dll|pyc|fuse_h*)$',
  \ }
let g:ctrlp_root_markers = ['.git', '.hg', 'trunk']
let g:ctrlp_dotfiles = 0

let g:vimfiler_as_default_explorer = 1

nnoremap <F2> :VimFilerExplorer<CR>
nnoremap <F3> :CtrlP<CR>
nnoremap <F4> :CtrlPBuffer<CR>
nnoremap <F5> :CtrlPTag<CR>


" call vimfiler#custom#profile('default', 'context', {
"  \ 'safe' : 0,
"  \ })

let g:vimfiler_no_default_key_mappings =  1

autocmd BufEnter vimfiler:* nmap <buffer> t <Plug>(vimfiler_loop_cursor_up)
autocmd BufEnter vimfiler:* nmap <buffer> h <Plug>(vimfiler_loop_cursor_down)
autocmd BufEnter vimfiler:* nmap <buffer> n <Plug>(vimfiler_smart_l)
autocmd BufEnter vimfiler:* nmap <buffer> d <Plug>(vimfiler_smart_h)


" Toggle mark.
autocmd BufEnter vimfiler:* nmap <buffer> <C-l> <Plug>(vimfiler_redraw_screen)
autocmd BufEnter vimfiler:* nmap <buffer> <Space> <Plug>(vimfiler_toggle_mark_current_line)
autocmd BufEnter vimfiler:* nmap <buffer> <S-LeftMouse> <Plug>(vimfiler_toggle_mark_current_line)
autocmd BufEnter vimfiler:* nmap <buffer> <S-Space> <Plug>(vimfiler_toggle_mark_current_line_up)
autocmd BufEnter vimfiler:* vmap <buffer> <Space> <Plug>(vimfiler_toggle_mark_selected_lines)


 " Toggle marks in all lines.
autocmd BufEnter vimfiler:* nmap <buffer> * <Plug>(vimfiler_toggle_mark_all_lines)
autocmd BufEnter vimfiler:* nmap <buffer> # <Plug>(vimfiler_mark_similar_lines)
" Clear marks in all lines.
autocmd BufEnter vimfiler:* nmap <buffer> U <Plug>(vimfiler_clear_mark_all_lines)
" Copy files.
autocmd BufEnter vimfiler:* nmap <buffer> c <Plug>(vimfiler_copy_file)
autocmd BufEnter vimfiler:* nmap <buffer> Cc <Plug>(vimfiler_clipboard_copy_file)
" Move files.
autocmd BufEnter vimfiler:* nmap <buffer> m <Plug>(vimfiler_move_file)
autocmd BufEnter vimfiler:* nmap <buffer> Cm <Plug>(vimfiler_clipboard_move_file)
" Delete files.
autocmd BufEnter vimfiler:* nmap <buffer> DD <Plug>(vimfiler_delete_file)
" Rename.
autocmd BufEnter vimfiler:* nmap <buffer> r <Plug>(vimfiler_rename_file)
" Make directory.
autocmd BufEnter vimfiler:* nmap <buffer> K <Plug>(vimfiler_make_directory)
" New file.
autocmd BufEnter vimfiler:* nmap <buffer> N <Plug>(vimfiler_new_file)
" Paste.
autocmd BufEnter vimfiler:* nmap <buffer> Cp <Plug>(vimfiler_clipboard_paste)


autocmd BufEnter vimfiler:* nmap <buffer> yy <Plug>(vimfiler_yank_full_path)
autocmd BufEnter vimfiler:* nmap <buffer> <Enter> <Plug>(vimfiler_expand_or_edit)
autocmd BufEnter vimfiler:* nmap <buffer> gs <Plug>(vimfiler_toggle_safe_mode)



autocmd BufEnter vimfiler:* nmap <buffer> <BS> <Plug>(vimfiler_switch_to_parent_directory)
 " Edit file.
autocmd BufEnter vimfiler:* nmap <buffer> e <Plug>(vimfiler_edit_file)
autocmd BufEnter vimfiler:* nmap <buffer> E <Plug>(vimfiler_split_edit_file)
autocmd BufEnter vimfiler:* nmap <buffer> B <Plug>(vimfiler_edit_binary_file)
" Choose action.
autocmd BufEnter vimfiler:* nmap <buffer> a <Plug>(vimfiler_choose_action)


 " Hide vimfiler.
autocmd BufEnter vimfiler:* nmap <buffer> q <Plug>(vimfiler_hide)
" Exit vimfiler.
autocmd BufEnter vimfiler:* nmap <buffer> Q <Plug>(vimfiler_exit)
" Close vimfiler.
autocmd BufEnter vimfiler:* nmap <buffer> - <Plug>(vimfiler_close)
autocmd BufEnter vimfiler:* nmap <buffer> <F2> <Plug>(vimfiler_close)
