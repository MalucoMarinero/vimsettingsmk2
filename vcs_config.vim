
NeoBundle 'tpope/vim-git'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'phleet/vim-mercenary'
NeoBundle 'ludovicchabant/vim-lawrencium'



" Git Fugitive Mapping
map <leader>g :Git
map <leader>gc :Gcommit<CR>
map <leader>gca :Gcommit --amend<CR>
map <leader>gd :Gdiff<CR>
map <leader>ga :Gwrite<CR>
map <leader>gs :Gstatus<CR>
map <leader>gh :Gcd<CR>
map <leader>gb :Gblame<CR>

" HG Mappings

map <leader>hb :HGblame
map <leader>hs :Hgstatus<CR>
map <leader>hd :Hgvdiff<CR>
map <leader>hc :Hgcommit<CR>
map <leader>hl :Hglog<CR>
map <leader>ha :Hgannotate<CR>


" VCS Command Mapping
let g:VCSCommandMapPrefix = '<leader>v'
