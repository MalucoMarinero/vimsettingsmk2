" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

if has('vim_starting')
  set nocompatible               " Be iMproved

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!

NeoBundle "Shougo/vimproc.vim"
NeoBundle 'Shougo/neocomplcache'


" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.


set t_Co=256
let g:Powerline_symbols = 'fancy'
set nu
set wrap
set cc=80
set cursorline
" gets rid of input lag with Esc key in terminal
set ttimeout
set ttimeoutlen=100
set number


so ~/.vim/base_keys.vim
so ~/.vim/editing_config.vim
so ~/.vim/unite_config.vim
so ~/.vim/vcs_config.vim

so ~/.vim/lang_config.vim
so ~/.vim/test_runner.vim

"" Winheight settings
set winwidth=84
set winheight=5
set winminheight=5
set winheight=99
set nowrap

set wildmenu
set wildmode=list:full

NeoBundleCheck
call neobundle#end()
colorscheme jellybeans

hi ColorColumn ctermbg=lightgrey guibg=lightgrey
:au WinEnter * 2mat ColorColumn '\%>80v.\+'

" Required:
filetype plugin indent on
syntax on



command! RefreshConfig so ~/.vim/vimrc
map <leader>rc :RefreshConfig<CR>
map <leader>rp :CtrlPClearAllCaches<CR>
