NeoBundle 'editorconfig/editorconfig-vim'
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']


" Hightlight bad chars
exec "set listchars=tab:\uBB\uBB,trail:\uB7,nbsp:~"
set list

" Tabular
NeoBundle "godlygeek/tabular"
map <Leader>t= :Tabularize /=<CR>
map <Leader>t: :Tabularize /:\zs<CR>



" Indent Guides
NeoBundle "nathanaelkane/vim-indent-guides"
let g:indent_guides_start_level=3
let g:indent_guides_guide_size=1
let g:indent_guides_auto_colors = 1

NeoBundle 'kien/rainbow_parentheses.vim'
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces



NeoBundle 'Valloric/YouCompleteMe'
NeoBundle 'SirVer/ultisnips'


let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"



let g:EclimCompletionMethod = 'omnifunc'


let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1
" let g:ycm_add_preview_to_completeopt = 1
let g:ycm_seed_identifiers_with_syntax = 1
set completeopt=menuone

set backspace=indent,eol,start


let tlist_coffee_settings='coffee;c:class;m:method;f:function'
let tlist_scss_settings='scss;m:mixin;d:definition;f:function;v:variable'

let g:airline_powerline_fonts = 1

if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif


NeoBundle 'vim-airline/vim-airline'
NeoBundle 'mileszs/ack.vim'
NeoBundle 'taglist.vim'
NeoBundle 'prettier/vim-prettier'
let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync

NeoBundle 'tpope/vim-surround'
NeoBundle 'tpope/vim-commentary'
NeoBundle 'tpope/vim-repeat'
NeoBundle 'tpope/vim-abolish'


nmap js  <Plug>Dsurround
nmap cs  <Plug>Csurround
nmap ys  <Plug>Ysurround
nmap yS  <Plug>YSurround
nmap yss <Plug>Yssurround
nmap ySs <Plug>YSsurround
nmap ySS <Plug>YSsurround
xmap S   <Plug>VSurround
xmap gS  <Plug>VgSurround

xmap <leader>~  <Plug>Commentary
nmap <leader>~  <Plug>Commentary
nmap <leader>~~ <Plug>CommentaryLine
" nmap <leader>~~u <Plug>CommentaryUndo

let g:surround_no_mappings = 1


" Taglist
nnoremap <F6> :TlistToggle<CR>
let g:Tlist_Show_One_File = 1
let g:Tlist_GainFocus_On_ToggleOpen = 1
map <leader>tn :tnext<CR>
map <leader>tp :tprev<CR>



