"""""""""""""""""""""""""""""""""""""""""""""""
"TEST RUNNER
"""""""""""""""""""""""""""""""""""""""""""""""

let t:js_test_runner = "mocha"
function! RunTests(filename)
    " Write the file and run tests for the given filename
    :w
    :silent !echo;echo;echo;echo;echo
    if match(a:filename, '.coffee$') != -1
        if t:js_test_runner == "mocha"
            exec ":!mocha --compilers coffee:coffee-script/register " . a:filename
        else
            exec ":!jasmine-node --coffee " . a:filename
        endif
    elseif match(a:filename, '.js$') != -1
        if t:js_test_runner == "mocha"
            exec ":!mocha --compilers js:6to5/register " . a:filename
        else
            exec ":!jasmine-node " . a:filename
        endif
    elseif match(a:filename, '.ts$') != -1
        let rootdir = fnamemodify(a:filename, ':h')
        exec ":!tsc --module commonjs --outDir " . rootdir . "/test " . a:filename
        let searchtest = substitute(fnamemodify(a:filename, ':t'), "ts$", "js", "")
        let test_filename = system('find ./' . rootdir . "/test -name '" . searchtest . "'")
	echom rootdir
	echom searchtest
        exec ":!mocha " . test_filename
        call system("rm -rf " . rootdir . "/test")
    elseif match(a:filename, '.scala$') != -1
        let sbt_dir = fnamemodify(system('find -name sbt'), ":h")
	echom "Found sbt"
	echom sbt_dir

        exec ":!cd " . sbt_dir . ";sbt 'testOnly " . t:test_package . ".*'"
    endif
endfunction

function! SetTestFile()
    " Set the spec file that tests will be run for.
    let t:test_file=@%
    if match(expand("%:t"), ".scala") != -1
        let t:test_package = substitute(getline(1), "package ", "", "")
    endif
endfunction

function! SetJSTestRunner()
    " Prompts for change to JS Test Runner
    let js_choice = confirm("Choose JS Test Runner", "mocha\njasmine", 1)
    if js_choice == 1
	echom "Set Runner to Mocha"
        let t:js_test_runner = "mocha"
    else
	echom "Set Runner to Jasmine"
        let t:js_test_runner = "jasmine"
    endif
endfunction

function! RunTestFile(...)
    if a:0
        let command_suffix = a:1
    else
        let command_suffix = ""
    endif

    " Run the tests for the previously-marked file.
    let in_spec_file = match(expand("%:t"), 'UnitTest\|unit_test\|unittest\|Spec\|spec') != -1
    echo expand('%')
    if in_spec_file
        call SetTestFile()
    elseif !exists("t:test_file")
        echom "No unit test file found or bound."
        return
    end
    call RunTests(t:test_file . command_suffix)
endfunction

function! RunNearestTest()
    let spec_line_number = line('.')
    call RunTestFile(":" . spec_line_number)
endfunction

" Run this file
map <leader>ut :call RunTestFile()<cr>
map <leader>utm :call SetTestFile()<cr>
map <leader>uts :call SetJSTestRunner()<cr>
" Run only the example under the cursor
map <leader>uT :call RunNearestTest()<cr>
" Run all test files
" map <leader>a :call RunTests('spec')<cr>
