let mapleader = '\'

for prefix in ['i', 'n', 'v']
  for key in ['<Up>', '<Down>', '<Left>', '<Right>']
    exe prefix . "noremap " . key . " <Nop>"
  endfor
endfor


no d h
no h j
no n l
no t k
no <a-h> gj
no <a-t> gk
no H 8j
no T 8k
no j d
no l n
no k t
no K T
no L N
no s :
no S :

no - $
no _ ^


map <c-h> <c-w>j
map <c-t> <c-w>k
map <c-n> <c-w>l
map <c-d> <c-w>h

map Q <Nop>
