setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab
setlocal autoindent
setlocal smartindent
setlocal smarttab
setlocal formatoptions=croq1

setlocal omnifunc=csscomplete#CompleteCSS
setlocal iskeyword +=-,$


execute 'RainbowParenthesesActivate'
execute 'RainbowParenthesesLoadBraces'

" inoremap <buffer><silent> : :<Esc>:call <SID>align()<CR>a
" function! s:align()
"   let reg = '^\s*[a-z\-]*:\s*'
"   if getline('.') =~# reg && (getline(line('.')-1) =~# reg || getline(line('.')+1) =~# reg)
"     Tabularize /^\s*[a-z\-]*:\zs
"     if getline(line('.')-1) =~# reg
"       let endpos = strlen(matchstr(getline(line('.')-1), reg))
"     else
"       let endpos = strlen(matchstr(getline(line('.')+1), reg))
"     endif
"     normal! 0
    
"     let currentlinelen = strlen(getline('.')) 
"     if currentlinelen < endpos
"       exe "normal A" . repeat(' ', endpos - currentlinelen) . "\<ESC>"
"     else
"       call cursor(line('.'), endpos)
"     endif
"   endif
" endfunction

