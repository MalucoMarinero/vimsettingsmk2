" Less
NeoBundle "groenewege/vim-less"

" Scala
" NeoBundle "mdreves/vim-scaladoc"
NeoBundle 'derekwyatt/vim-scala'

"REST
NeoBundle 'diepm/vim-rest-console'
let s:vrc_auto_format_response_patterns = {
\ 'json': 'python -m json.tool',
\ 'application/json': 'python -m json.tool',
\ 'xml': 'xmllint --format -',
\}


" Javascript Modes
NeoBundle 'pangloss/vim-javascript'

" Coffeescript
NeoBundle 'kchmck/vim-coffee-script'

" Typescript
NeoBundle 'leafgarland/typescript-vim'
NeoBundle 'clausreinke/typescript-tools.vim'
NeoBundle 'Quramy/tsuquyomi'

" Haskell Modes
NeoBundle 'eagletmt/ghcmod-vim'
NeoBundle 'lukerandall/haskellmode-vim'
NeoBundle 'ujihisa/neco-ghc'
let g:haddock_browser = "firefox"

" Elm Modes
NeoBundle "lambdatoast/elm.vim"

" Jade
NeoBundle 'digitaltoad/vim-jade'

" Salt
NeoBundle "saltstack/salt-vim"

" Elixir
NeoBundle 'elixir-editors/vim-elixir'
NeoBundle 'slashmili/alchemist.vim'

" Stylus
NeoBundle 'wavded/vim-stylus'


" json
NeoBundle 'tpope/vim-jdaddy'

" SASS
autocmd BufNewFile,BufRead *.sass             set ft=sass.css
autocmd BufNewFile,BufRead *.lass             set ft=stylus
autocmd BufNewFile,BufRead *.pp               set ft=puppet

function! s:MKDir(...)
    if         !a:0
           \|| stridx('`+', a:1[0])!=-1
           \|| a:1=~#'\v\\@<![ *?[%#]'
           \|| isdirectory(a:1)
           \|| filereadable(a:1)
           \|| isdirectory(fnamemodify(a:1, ':p:h'))
        return
    endif
    return mkdir(fnamemodify(a:1, ':p:h'), 'p')
endfunction
command! -bang -bar -nargs=? -complete=file E :call s:MKDir(<f-args>) | e<bang> <args>

